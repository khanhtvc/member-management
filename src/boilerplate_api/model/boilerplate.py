from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from boilerplate_api import config


class BoilerplateModel(GinoBaseModel):
    __tablename__ = "boilerplate"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    name = db.Column(db.String)
    description = db.Column(db.String)


class AccountModel(GinoBaseModel):
    __tablename__ = "account"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    customer_id = db.Column(db.String)
    bank_id = db.Column(db.String)
    password = db.Column(db.String)
    balance = db.Column(db.Integer)


class CustomerModel(GinoBaseModel):
    __tablename__ = "customer"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    indentity_number = db.Column(db.String)
    birth_date = db.Column(db.DateTime)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    street_name = db.Column(db.String)
    street_numeral = db.Column(db.Integer)
    phone_number = db.Column(db.String)
    email = db.Column(db.String)


class TransactionModel(GinoBaseModel):
    __tablename__ = "transaction"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    account_id = db.Column(db.String)
    time_start  = db.Column(db.DateTime)
    atm_machine_id = db.Column(db.String)
    amount = db.Column(db.Integer)
    type_id = db.Column(db.Integer)
    destination_account_id = db.Column(db.String)