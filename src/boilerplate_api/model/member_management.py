from sqlalchemy.dialects.postgresql import UUID
from fii_cqrs.backend.gino import GinoBaseModel, db
from boilerplate_api.domain.datadef import GenderType, ContactType
from sqlalchemy.dialects.postgresql import ARRAY
from boilerplate_api import config
from fii_cqrs.activity import ActivityType


class MemberModel(GinoBaseModel):
    __tablename__ = "member"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    member_key = db.Column(db.String)
    picture_id = db.Column(UUID)
    name__family = db.Column(db.String)
    name__middle = db.Column(db.String)
    name__given = db.Column(db.String)
    name__prefix = db.Column(db.String)
    name__suffix = db.Column(db.String)
    gender = db.Column(db.Enum(GenderType))
    hicn = db.Column(db.String)
    language = db.Column(ARRAY(db.String(255)))
    birthdate = db.Column(db.DateTime)
    tags = db.Column(ARRAY(db.String(255)))


class ContactModel(GinoBaseModel):
    __tablename__ = "contact"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    member_id = db.Column(UUID)
    type = db.Column(db.Enum(ContactType))
    full_name = db.Column(db.String)
    relation = db.Column(db.String)
    address__line1 = db.Column(db.String)
    address__line2 = db.Column(db.String)
    address__city = db.Column(db.String)
    address__country = db.Column(db.String)
    address__postal = db.Column(db.String)
    address__state = db.Column(db.String)
    telecom__phone1 = db.Column(db.String)
    telecom__phone2 = db.Column(db.String)
    telecom__email = db.Column(db.String)
    telecom__fax = db.Column(db.String)
    pager = db.Column(db.String)


class IdentifierModel(GinoBaseModel):
    __tablename__ = "identifier"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    member_id = db.Column(UUID)
    type = db.Column(db.String)
    number = db.Column(db.Integer)
    identifier_use = db.Column(db.String)
    assigner = db.Column(db.String)
    effective_period_start = db.Column(db.DateTime)
    effective_period_end = db.Column(db.DateTime)


class EligibilityModel(GinoBaseModel):
    __tablename__ = "eligibility"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    member_id = db.Column(UUID)
    subscriber_id = db.Column(db.String)
    plan_code = db.Column(db.String)
    eligibility_start = db.Column(db.DateTime)
    eligibility_end = db.Column(db.DateTime)
    coverage_start = db.Column(db.DateTime)
    coverage_end = db.Column(db.DateTime)
    identification_code_qualifier = db.Column(db.String)
    maintenance_type_code = db.Column(db.String)
    maintenance_reason_code = db.Column(db.String)
    policy_number = db.Column(db.String)
    provider_effective_date = db.Column(db.DateTime)
    provider_npi = db.Column(db.String)
    tax_id = db.Column(db.String)
    mbi = db.Column(db.String)

class DocumentModel(GinoBaseModel):
    __tablename__ = "eligibility-document"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    document_id = db.Column(UUID)
    eligibility_id = db.Column(UUID)

class ActivityLogModel(GinoBaseModel):
    __tablename__ = "activity-log"
    __table_args__ = dict(schema=config.BOILERPLATE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    message = db.Column(db.String())
    data = db.Column(db.JSON)