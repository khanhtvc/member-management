from pyrsistent import field
from datetime import datetime
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
import enum
from sanic_cqrs import PostgresCqrsResource
from boilerplate_api.model import MemberModel, ActivityLogModel, IdentifierModel, ContactModel, EligibilityModel, DocumentModel 
from boilerplate_api.domain.datadef import GenderType, ContactType

@cr.register("member")
class MemberResource(PostgresCqrsResource):
    __backend__ = MemberModel

    _id = field(type=UUID_TYPE)
    member_key = field(type=nullable(str))
    picture_id = field(type=UUID_TYPE)
    name__family = field(type=nullable(str))
    name__middle = field(type=nullable(str))
    name__given = field(type=nullable(str))
    name__prefix = field(type=nullable(str))
    name__suffix = field(type=nullable(str))
    gender = field(type=nullable(GenderType))
    hicn = field(type=nullable(str))
    language = field(type=nullable(list))
    birthdate = field(type=nullable(datetime))
    tags = field(type=nullable(list))

@cr.register("identifier")
class IdentifierResource(PostgresCqrsResource):
    __backend__ = IdentifierModel

    _id = field(type=UUID_TYPE)
    member_id = field(type=UUID_TYPE)
    type = field(type=nullable(str))
    number = field(type=nullable(int))
    identifier_use = field(type=nullable(str))
    assigner = field(type=nullable(str))
    effective_period_start = field(type=nullable(datetime))
    effective_period_end = field(type=nullable(datetime))

@cr.register("contact")
class ContactResource(PostgresCqrsResource):
    __backend__ = ContactModel

    _id = field(type=UUID_TYPE)
    member_id = field(type=UUID_TYPE)
    type = field(type=nullable(ContactType))
    full_name = field(type=nullable(str))
    relation = field(type=nullable(str))
    address__line1 = field(type=nullable(str))
    address__line2 = field(type=nullable(str))
    address__city = field(type=nullable(str))
    address__country = field(type=nullable(str))
    address__postal = field(type=nullable(str))
    address__state = field(type=nullable(str))
    telecom__phone1 = field(type=nullable(str))
    telecom__phone2 = field(type=nullable(str))
    telecom__email = field(type=nullable(str))
    telecom__fax = field(type=nullable(str))
    pager = field(type=nullable(str))

@cr.register("eligibility")
class EligibilityResource(PostgresCqrsResource):
    __backend__ = EligibilityModel

    _id = field(type=UUID_TYPE)
    member_id = field(type=UUID_TYPE)
    subscriber_id = field(type=nullable(str))
    plan_code = field(type=nullable(str))
    eligibility_start = field(type=nullable(datetime))
    eligibility_end = field(type=nullable(datetime))
    coverage_start = field(type=nullable(datetime))
    coverage_end = field(type=nullable(datetime))
    identification_code_qualifier = field(type=nullable(str))
    maintenance_type_code = field(type=nullable(str))
    maintenance_reason_code = field(type=nullable(str))
    policy_number = field(type=nullable(str))
    provider_effective_date = field(type=nullable(datetime))
    provider_npi = field(type=nullable(str))
    tax_id = field(type=nullable(str))
    mbi = field(type=nullable(str))

@cr.register("document")
class IdentifierResource(PostgresCqrsResource):
    __backend__ = DocumentModel

    _id = field(type=UUID_TYPE)
    eligibility_id = field(type=UUID_TYPE)
    document_id = field(type=UUID_TYPE)


@cr.register("activitylog")
class IdentifierResource(PostgresCqrsResource):
    __backend__ = ActivityLogModel

    _id = field(type=UUID_TYPE)
    message = field(type=nullable(str))
    data = field(type=nullable(dict))