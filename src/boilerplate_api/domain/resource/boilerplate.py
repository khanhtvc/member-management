from pyrsistent import field
from datetime import datetime
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from boilerplate_api.model import BoilerplateModel, AccountModel, CustomerModel, TransactionModel


@cr.register("boilerplate")
class BoilerplateResource(PostgresCqrsResource):
    __backend__ = BoilerplateModel

    _id = field(type=UUID_TYPE)
    name = field(type=nullable(str))
    description = field(type=nullable(str))

@cr.register("account")
class AccountResource(PostgresCqrsResource):
    __backend__ = AccountModel

    _id = field(type=UUID_TYPE)
    customer_id = field(type=nullable(str))
    bank_id = field(type=nullable(str))
    password = field(type=nullable(str))
    balance = field(type=nullable(int))

@cr.register("customer")
class CustomerResource(PostgresCqrsResource):
    __backend__ = CustomerModel

    _id = field(type=UUID_TYPE)
    indentity_number = field(type=nullable(str))
    birth_date = field(type=nullable(datetime))
    first_name = field(type=nullable(str))
    last_name = field(type=nullable(str))
    street_name = field(type=nullable(str))
    street_numeral = field(type=nullable(int))
    phone_number = field(type=nullable(str))
    email =field(type=nullable(str))

@cr.register("transaction")
class TransactionResource(PostgresCqrsResource):
    __backend__ = TransactionModel

    _id = field(type=UUID_TYPE)
    account_id = field(type=nullable(str))
    time_start = field(type=nullable(datetime))
    atm_machine_id = field(type=nullable(str))
    amount = field(type=nullable(int))
    type_id = field(type=nullable(int))
    destination_account_id = field(type=nullable(str))
