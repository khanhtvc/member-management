from pyrsistent import field
from datetime import datetime
import enum
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring
from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE

# define enum


class GenderType(enum.Enum):
    male = "male"
    female = "female"
    other = "other"


class ContactType(enum.Enum):
    primary_contact = 'primary contact'
    other_contact = 'other contact'
    emergency_contact = 'emergency contact'

def parse_contact(input):
    if input in ("primary contact", "Primary Contact", "PRIMARY CONTACT"):
        return ContactType.primary_contact
    elif input in ("other contact", "Other Contact", "OTHER CONTACT"):
        return ContactType.other_contact
    elif input in ("emergency contact", "Emergency Contact", "EMERGENCY CONTACT"):
        return ContactType.emergency_contact
    else:
        raise NotImplementedError

def parse_enum_string(input):
    if input in ("MALE", "male", "Male"):
        return GenderType.male
    elif input in ("female", "FEMALE", "Female"):
        return GenderType.female
    elif input in ("other", "OTHER", "Other"):
        return GenderType.other
    else:
        raise NotImplementedError


class CreateMemberData(CommandData):
    member_key = field(type=str, mandatory=True)
    picture_id = field(type=UUID_TYPE, mandatory=True, factory=to_uuid)
    name__family = field(type=str, mandatory=True)
    name__middle = field(type=str, mandatory=True)
    name__given = field(type=str, mandatory=True)
    name__prefix = field(type=str, mandatory=True)
    name__suffix = field(type=str, mandatory=True)
    gender = field(type=GenderType, mandatory=True, factory=parse_enum_string)
    hicn = field(type=str, mandatory=True)
    language = field(type=list, mandatory=True)
    birthdate = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    tags = field(type=list, mandatory=True)


class CreateMemberEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    member_key = field(type=str, mandatory=True)
    picture_id = field(type=UUID_TYPE, mandatory=True)
    name__family = field(type=str, mandatory=True)
    name__middle = field(type=str, mandatory=True)
    name__given = field(type=str, mandatory=True)
    name__prefix = field(type=str, mandatory=True)
    name__suffix = field(type=str, mandatory=True)
    gender = field(type=GenderType, mandatory=True)
    hicn = field(type=str, mandatory=True)
    language = field(type=list, mandatory=True)
    birthdate = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    tags = field(type=list, mandatory=True)


class CreateIdentifierData(CommandData):
    type = field(type=str, mandatory=True)
    number = field(type=int, mandatory=True)
    identifier_use = field(type=str, mandatory=True)
    assigner = field(type=str, mandatory=True)
    effective_period_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    effective_period_end = field(type=datetime, mandatory=True, factory=parse_iso_datestring)

class CreateIdentifierEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    member_id = field(type=UUID_TYPE, mandatory=True, factory=to_uuid)
    type = field(type=str, mandatory=True)
    number = field(type=int, mandatory=True)
    identifier_use = field(type=str, mandatory=True)
    assigner = field(type=str, mandatory=True)
    effective_period_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    effective_period_end = field(type=datetime, mandatory=True, factory=parse_iso_datestring)


class CreateContactData(CommandData):
    type = field(type=ContactType, mandatory=True, factory=parse_contact)
    full_name = field(type=str, mandatory=True)
    relation = field(type=str, mandatory=True)
    address__city = field(type=str, mandatory=True)
    address__country = field(type=str, mandatory=True)
    address__line1 = field(type=str, mandatory=True)
    address__line2 = field(type=str, mandatory=True)
    address__postal = field(type=str, mandatory=True)
    address__state = field(type=str, mandatory=True)
    telecom__phone1 = field(type=str, mandatory=True)
    telecom__phone2 = field(type=str, mandatory=True)
    telecom__email = field(type=str, mandatory=True)
    telecom__fax = field(type=str, mandatory=True)
    pager = field(type=str, mandatory=True)

class CreateContactEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    member_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    type = field(type=ContactType, mandatory=True)
    full_name = field(type=str, mandatory=True)
    relation = field(type=str, mandatory=True)
    address__city = field(type=str, mandatory=True)
    address__country = field(type=str, mandatory=True)
    address__line1 = field(type=str, mandatory=True)
    address__line2 = field(type=str, mandatory=True)
    address__postal = field(type=str, mandatory=True)
    address__state = field(type=str, mandatory=True)
    telecom__phone1 = field(type=str, mandatory=True)
    telecom__phone2 = field(type=str, mandatory=True)
    telecom__email = field(type=str, mandatory=True)
    telecom__fax = field(type=str, mandatory=True)
    pager = field(type=str, mandatory=True)





class CreateEligibilityData(CommandData):
    subscriber_id = field(type=str, mandatory=True)
    plan_code = field(type=str, mandatory=True)
    eligibility_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    eligibility_end = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    coverage_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    coverage_end = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    identification_code_qualifier  = field(type=str, mandatory=True)
    maintenance_type_code = field(type=str, mandatory=True)
    maintenance_reason_code = field(type=str, mandatory=True)
    policy_number = field(type=str, mandatory=True)
    provider_effective_date = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    provider_npi = field(type=str, mandatory=True)
    tax_id = field(type=str, mandatory=True)
    mbi = field(type=str, mandatory=True)

class CreateEligibilityEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    member_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    subscriber_id = field(type=str, mandatory=True)
    plan_code = field(type=str, mandatory=True)
    eligibility_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    eligibility_end = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    coverage_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    coverage_end = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    identification_code_qualifier  = field(type=str, mandatory=True)
    maintenance_type_code = field(type=str, mandatory=True)
    maintenance_reason_code = field(type=str, mandatory=True)
    policy_number = field(type=str, mandatory=True)
    provider_effective_date = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    provider_npi = field(type=str, mandatory=True)
    tax_id = field(type=str, mandatory=True)
    mbi = field(type=str, mandatory=True)


class CreateDocumentData(CommandData):
    document_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

class CreateDocumentEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    document_id = field(type=UUID_TYPE, mandatory=True)
    eligibility_id = field(type=UUID_TYPE, mandatory=True, factory=to_uuid)

class CreateActivityLogData(CommandData):
    message = field(type=str, mandatory=True)
    data = field(type=dict, mandatory=True)

class CreateActivityLogEventData(EventData):
    _id = field(type=UUID_TYPE, mandatory=True, factory=to_uuid)

    message = field(type=str, mandatory=True)
    data = field(type=dict, mandatory=True)