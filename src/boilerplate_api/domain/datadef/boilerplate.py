from pyrsistent import field
from datetime import datetime
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE


class CreateBoilerplateData(CommandData):
    name = field(type=str, mandatory=True)
    description = field(type=str, mandatory=True)


class CreateBoilerplateEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    name = field(type=str, mandatory=True)
    description = field(type=str, mandatory=True)


class CreateAccountData(CommandData):
    customer_id = field(type=str, mandatory=True)
    bank_id = field(type=str, mandatory=True)
    password = field(type=str, mandatory=True)
    balance = field(type=int, mandatory=True)
    # color = field(type=Color, mandatory=True)

class CreateAccountEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    customer_id = field(type=str, mandatory=True)
    bank_id = field(type=str, mandatory=True)
    password = field(type=str, mandatory=True)
    balance = field(type=int, mandatory=True)

class CreateCustomerData(CommandData):
    indentity_number = field(type=str, mandatory=True)
    birth_date = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    first_name = field(type=str, mandatory=True)
    last_name = field(type=str, mandatory=True)
    street_name = field(type=str, mandatory=True)
    street_numeral = field(type=int, mandatory=True)
    phone_number = field(type=str, mandatory=True)
    email = field(type=str, mandatory=True)

class CreateCustomerEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    indentity_number = field(type=str, mandatory=True)
    birth_date= field(type=datetime, mandatory=True)
    first_name = field(type=str, mandatory=True)
    last_name = field(type=str, mandatory=True)
    street_name = field(type=str, mandatory=True)
    street_numeral = field(type=int, mandatory=True)
    phone_number = field(type=str, mandatory=True)
    email = field(type=str, mandatory=True)

class CreateTransactionData(CommandData):

    account_id = field(type=str, mandatory=True)
    atm_machine_id = field(type=str, mandatory=True)
    type_id = field(type=int, mandatory=True)
    time_start = field(type=datetime, mandatory=True, factory=parse_iso_datestring)
    amount = field(type=int, mandatory=True, invariant=lambda p: (p > 0, 'amount must > 0'))
    destination_account_id = field(type=str, mandatory=True)

class CreateTransactionEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    account_id = field(type=str, mandatory=True)
    atm_machine_id = field(type=str, mandatory=True)
    type_id = field(type=int, mandatory=True)
    time_start = field(type=datetime, mandatory=True)
    amount = field(type=int, mandatory=True)
    destination_account_id = field(type=str, mandatory=True)