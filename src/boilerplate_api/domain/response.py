from pyrsistent import PClass

from fii_cqrs.command import field
from fii_cqrs.response import CqrsResponse

from .domain import BoilerplateDomain

_entity = BoilerplateDomain.entity


def to_dict(data):
    if isinstance(data, PClass):
        return data.serialize()

    return data


@_entity("boilerplate-response")
class BoilerplateResponse(CqrsResponse):
    data = field(type=dict, factory=to_dict, mandatory=True)
