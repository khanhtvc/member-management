from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord, UpdateRecord

from boilerplate_api.domain.datadef import CreateMemberEventData, CreateContactEventData, CreateIdentifierEventData, CreateEligibilityEventData, CreateDocumentEventData, CreateActivityLogEventData
from ..domain import BoilerplateDomain

_entity = BoilerplateDomain.entity
_committer = BoilerplateDomain.event_committer


@_entity
class MemberCreated(Event):
    data = field(type=CreateMemberEventData, mandatory=True)


@_committer(MemberCreated)
async def process__member_created(statemgr, event):
    print("insert record")
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity
class IdentifierCreated(Event):
    data = field(type=CreateIdentifierEventData, mandatory=True)


@_committer(IdentifierCreated)
async def process__identifier_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity
class ContactCreated(Event):
    data = field(type=CreateContactEventData, mandatory=True)


@_committer(ContactCreated)
async def process__contact_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity
class EligibilityCreated(Event):
    data = field(type=CreateEligibilityEventData, mandatory=True)


@_committer(EligibilityCreated)
async def process__eligibility_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity
class DocumentCreated(Event):
    data = field(type=CreateDocumentEventData, mandatory=True)


@_committer(DocumentCreated)
async def process__document_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)


@_entity
class MemberUpdated(Event):
    data = field(type=CreateMemberEventData, mandatory=True)


@_committer(MemberUpdated)
async def process__member_updated(statemgr, event):
    print("event ok \n\n\n")
    yield UpdateRecord(resource=event.target.resource, data=event.data, identifier=event.target.identifier)
