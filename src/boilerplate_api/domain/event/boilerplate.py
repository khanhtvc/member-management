from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord

from boilerplate_api.domain.datadef import CreateMemberEventData, CreateContactEventData, CreateIdentifierEventData, CreateEligibilityEventData
from ..domain import BoilerplateDomain

_entity = BoilerplateDomain.entity
_committer = BoilerplateDomain.event_committer


@_entity
class BoilerplateCreated(Event):
    data = field(type=CreateBoilerplateEventData, mandatory=True)


@_committer(BoilerplateCreated)
async def process__boilerplate_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity
class AccountCreated(Event):
    data = field(type=CreateAccountEventData, mandatory=True)


@_committer(AccountCreated)
async def process__account_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity
class CustomerCreated(Event):
    data = field(type=CreateCustomerEventData, mandatory=True)


@_committer(CustomerCreated)
async def process__customer_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)


@_entity
class TransactionCreated(Event):
    data = field(type=CreateTransactionEventData, mandatory=True)


@_committer(TransactionCreated)
async def process__transaction_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)
