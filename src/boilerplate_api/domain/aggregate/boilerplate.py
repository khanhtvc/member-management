from fii_cqrs.aggregate import Aggregate
from fii_cqrs.event import Event
from fii_cqrs.identifier import UUID_GENR

from boilerplate_api.domain.datadef import CreateBoilerplateData, CreateAccountData, CreateCustomerData, CreateTransactionData #noqa
from boilerplate_api.domain.datadef import CreateBoilerplateEventData, CreateAccountEventData, CreateCustomerEventData, CreateTransactionEventData  # noqa


class BoilerplateAggregate(Aggregate):
    async def do__create_boilerplate(
        self, data: CreateBoilerplateData
    ) -> Event:
        event_data = CreateBoilerplateEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "boilerplate-created", target=self.aggroot, data=event_data
        )

    async def do__create_account(
        self, data: CreateAccountData
    ) -> Event:
        event_data = CreateAccountEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "account-created", target=self.aggroot, data=event_data
        )
    async def do__create_customer(
        self, data: CreateCustomerData
    ) -> Event:
        event_data = CreateCustomerEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "customer-created", target=self.aggroot, data=event_data
        )
    async def do__create_transaction(
        self, data: CreateTransactionData
    ) -> Event:
        event_data = CreateTransactionEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "transaction-created", target=self.aggroot, data=event_data
        )
    
    