from fii_cqrs.aggregate import Aggregate
from fii_cqrs.event import Event
from fii_cqrs.identifier import UUID_GENR
import json
from json import JSONEncoder
from boilerplate_api.domain.datadef import CreateMemberData, CreateContactData, CreateIdentifierData, CreateEligibilityData, CreateDocumentData, CreateActivityLogEventData, CreateActivityLogData #noqa
from boilerplate_api.domain.datadef import CreateMemberEventData,CreateContactEventData, CreateIdentifierEventData, CreateEligibilityEventData, CreateDocumentEventData # noqa
from fii_cqrs.backend.gino import PostgresActivityLogger

class MemberManagementAggregate(Aggregate):
    async def do__create_member(
        self, data: CreateMemberData
    ) -> Event:
        event_data = CreateMemberEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "member-created", target=self.aggroot, data=event_data #??
        )

    async def do__create_identifier(
        self, data: CreateIdentifierData
    ) -> Event:
        event_data = CreateIdentifierEventData.extend_pclass(
            pclass=data, _id=UUID_GENR(), member_id=self.aggroot.identifier
        )
        print("print aggroot", self.aggroot)
        return self.create_event(
            "identifier-created", target={"resource": "identifier", "identifier": self.aggroot.identifier}, data=event_data
        )

    async def do__create_contact(
        self, data: CreateContactData
    ) -> Event:
        event_data = CreateContactEventData.extend_pclass(
            pclass=data, _id=UUID_GENR(), member_id=self.aggroot.identifier
        )
        return self.create_event(
            "contact-created", target={"resource": "contact", "identifier": self.aggroot.identifier}, data=event_data
        )

    

    async def do__create_eligibility(
        self, data: CreateEligibilityData
    ) -> Event:
        event_data = CreateEligibilityEventData.extend_pclass(
            pclass=data, _id=UUID_GENR(), member_id=self.aggroot.identifier
        )
        return self.create_event(
            "eligibility-created", target={"resource": "eligibility", "identifier": self.aggroot.identifier}, data=event_data
        )

    async def do__create_document(
        self, data: CreateDocumentData
    ) -> Event:
        event_data = CreateDocumentEventData.extend_pclass(
            pclass=data, _id=UUID_GENR(), eligibility_id=self.aggroot.identifier
        )
        print("print aggroot", self.aggroot)
        return self.create_event(
            "document-created", target={"resource": "document", "identifier": UUID_GENR()}, data=event_data
        )


    async def do__update_member(
        self, data: CreateMemberData
    ) -> Event:
        print("aggregate ok \n\n\n")
        event_data = CreateMemberEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "member-updated", target=self.aggroot, data=event_data #??
        )
