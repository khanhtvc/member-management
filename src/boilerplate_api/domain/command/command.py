from fii_cqrs.command import Command, field
from fii import timeutil
import uuid
from datetime import datetime
from boilerplate_api.domain.datadef import CreateMemberData, CreateIdentifierData, CreateEligibilityData,CreateContactData, CreateDocumentData
from ..domain import BoilerplateDomain
from ..datadef import GenderType
_entity = BoilerplateDomain.entity
_handler = BoilerplateDomain.command_handler

    
@_entity("create-member")
class CreateMember(Command):
    data = field(type=CreateMemberData)

    class Meta:
        resource = "member"
        tags = ["member"]
        description = "Create new member"

@_handler(CreateMember)
async def handle_create_member(aggproxy, cmd: CreateMember):
    event = await aggproxy.create_member(cmd.data)
    yield event
    yield await aggproxy.create_response(
        "boilerplate-response", cmd, {"_id": event.data._id}
    )

@_entity("create-contact")
class CreateContact(Command):
    data = field(type=CreateContactData)

    class Meta:
        resource = "member/{member_id}"
        parameters = [
            {
                "name": "member_id", 
                "in": "path",
                "description": "Member ID",
                "required": True,
                "schema": {
                    "type": "UUID", 
                },
            }
        ]
        tags = ["contact"]
        description = "Create new contact"

@_handler(CreateContact)
async def handle_create_contact(aggproxy, cmd: CreateContact):
    event = await aggproxy.create_contact(cmd.data)
    yield event
    yield await aggproxy.create_response(
        "boilerplate-response", cmd, {"_id": event.data._id}
    )

@_entity("create-identifier")
class CreateIdentifier(Command):
    data = field(type=CreateIdentifierData)
    class Meta:
        resource = "member/{member_id}"
        parameters = [
            {
                "name": "member_id", 
                "in": "path",
                "description": "Member ID",
                "required": True,
                "schema": {
                    "type": "UUID", 
                },
            }
        ]
        tags = ["identifier"]
        description = "Create new identifier"

@_handler(CreateIdentifier)
async def handle_create_identifier(aggproxy, cmd: CreateIdentifier):
    event = await aggproxy.create_identifier(cmd.data)
    yield event
    yield await aggproxy.create_response(
        "boilerplate-response", cmd, {"_id": event.data._id}
    )

@_entity("create-eligibility")
class CreateEligibility(Command):
    data = field(type=CreateEligibilityData)

    class Meta:
        resource = "member/{member_id}"
        parameters = [
            {
                "name": "member_id", 
                "in": "path",
                "description": "Member ID",
                "required": True,
                "schema": {
                    "type": "UUID", 
                },
            }
        ]
        tags = ["eligibility"]
        description = "Create new eligibility"

@_handler(CreateEligibility)
async def handle_create_eligibility(aggproxy, cmd: CreateEligibility):
    event = await aggproxy.create_eligibility(cmd.data)
    yield event
    yield await aggproxy.create_response(
        "boilerplate-response", cmd, {"_id": event.data._id}
    )

@_entity("create-document")
class CreateDocument(Command):
    data = field(type=CreateDocumentData)
    class Meta:
        resource = "eligibility/{eligibility_id}"
        parameters = [
            {
                "name": "eligibility_id", 
                "in": "path",
                "description": "Eligibility ID",
                "required": True,
                "schema": {
                    "type": "UUID", 
                },
            }
        ]
        tags = ["Document"]
        description = "Create new document"

@_handler(CreateDocument)
async def handle_create_document(aggproxy, cmd: CreateDocument):
    event = await aggproxy.create_document(cmd.data)
    yield event
    yield await aggproxy.create_response(
        "boilerplate-response", cmd, {"_id": event.data._id}
    )

@_entity("update-member")
class UpdateMember(Command):
    data = field(type=CreateMemberData)

    class Meta:
        resource = "member/{member_id}"
        parameters = [
            {
                "name": "member_id", 
                "in": "path",
                "description": "Member ID",
                "required": True,
                "schema": {
                    "type": "UUID", 
                },
            }
        ]
        tags = ["member"]
        description = "Update member"

@_handler(UpdateMember)
async def handle_update_member(aggproxy, cmd: UpdateMember):
    event = await aggproxy.update_member(cmd.data)
    yield event
    data = event.data.serialize()
    def GenderType_to_string(gender):
        if gender == GenderType.male:
            return "male"
        elif gender == GenderType.female:
            return "female"
        elif gender == GenderType.other:
            return "other"

    for x, y in data.items():
        if type(y) is uuid.UUID:
            data[x] = str(y)
        if type(y) is GenderType:
            data[x] = GenderType_to_string(y)
        if type(y) is datetime:
            data[x] = timeutil.datetime_to_str(y)

    yield await aggproxy.log_activity(
        logroot={
            "identifier": cmd.aggroot.identifier,
            "resource": cmd.aggroot.resource,
            "namespace": "gotecq.member-management",
        },
        message="has update member",
        data=data,
        msglabel="update-member",
    )
    yield await aggproxy.create_response(
        "boilerplate-response", cmd, {"_id": event.data._id}
    )