from sanic_query import field as f
from sanic_query.resource import QueryResource


class BaseQueryResource(QueryResource):
    __soft_delete__ = "_deleted"

    _id = f.StringField("ID", identifier=True)
    _deleted = f.StringField("Deleted")
    _created = f.StringField("Created")
    _updated = f.StringField("Updated")
    _etag = f.StringField("Etag")
    _creator = f.UUIDField("Creator")
    _updater = f.UUIDField("Updater")


class BoilerplateQuery(BaseQueryResource):
    __table__ = "boilerplate"

    _id = f.StringField("ID", identifier=True)
    name = f.StringField("Name")
    description = f.StringField("Description")

    class Meta:
        tags = ["Boilerplate"]
        description = "Get boilerplate data"


class AccountQuery(BaseQueryResource):
    __table__ = "account"
    __endpoint__ = (
        "account/<customer_id>"
    )

    _id = f.StringField("ID", identifier=True)
    customer_id = f.StringField("Customer")
    bank_id = f.StringField("Bank")
    password = f.StringField("Password")
    balance = f.FloatField("Balance")

    class Meta:
        tags = ["Account"]
        description = "Get account data"
        parameters = [
            {
                "name": "customer_id",
                "in": "path",
                "description": "Customer ID",
                "required": True,
                "schema": {
                    "type": "string",
                },
            }
        ]

class CustomerQuery(BaseQueryResource):
    __table__ = "customer"
    __endpoint__ = (
        "customer/<first_name>"
    )

    _id = f.StringField("ID", identifier=True)
    indentity_number = f.StringField("IndentityNumber")
    birth_date = f.StringField("Birthdate")
    first_name = f.StringField("FirstName")
    last_name = f.StringField("LastName")
    street_name = f.StringField("StreetName")
    street_numeral = f.StringField("StreetNumeral")
    phone_number = f.StringField("PhoneNumber")
    email = f.StringField("Email")

    class Meta:
        tags = ["Customer"]
        description = "Get customer data"
        parameters = [
            {
                "name": "first_name",
                "in": "path",
                "description": "First Name",
                "required": True,
                "schema": {
                    "type": "string", 
                },
            }
        ]
