from sanic_connector import configure_connector
from sanic_kcauth import configure_kcauth
from sanic_swagger import configure_sanic_swagger
from sanic_toolbox import configure_toolbox

from sanic_cqrs import configure_cqrs

from boilerplate_api.domain import configure_domain
from boilerplate_api.query import configure_query, account_query, customer_query
from sanic import Sanic
from .cfg import config

app = Sanic(config.APPLICATION_NAME, configure_logging=False)

app.config.update(config.items())
configure_connector(app)
configure_domain(app)
configure_kcauth(app)
configure_cqrs(app, config.APPLICATION_NAME)
configure_toolbox(app)
configure_sanic_swagger(app, config.OPENAPI_EXPOSE_SWAGGER_DOCS)
configure_query(app)
customer_query(app)
account_query(app)
