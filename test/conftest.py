import os
import pytest
from datetime import datetime

BASE_URL = "https://boilerplate-api.local.fiisoft.net/"
SESSION = "893623d98660beeddcd1710f961ae047632e14c1c227233293939290ab34a34d7b796d14e84861e2"
TEST_ID = datetime.now().strftime("%Y%m%d-%H:%M:%S")
SUCCESS_CODE = [200, 201, 202, 203, 204, 205, 206, 207, 208, 226]


@pytest.fixture
def construct_url():
    def join(*args):
        return os.path.join(BASE_URL, *args)

    return join


@pytest.fixture
def auth_cookie():
    return {"session": SESSION}


@pytest.fixture
def test_id():
    return TEST_ID


@pytest.fixture
def success_code():
    return SUCCESS_CODE
