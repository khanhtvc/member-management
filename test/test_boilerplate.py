import pytest
import requests

from fii.log import logger


def log_response(resp):
    logger.debug("Status: %s", resp.status_code)
    logger.debug("Header: %s", resp.headers)
    logger.debug("Json Response: %s", resp.json())
    logger.debug("Raw text: %s", resp.text)


class TestboilerplateAPI:
    result = {}

    @property
    def session(self):
        if hasattr(self, "_session"):
            return self._session
        self._session = requests.Session()
        self._session.verify = False
        return self._session

    @pytest.mark.debug
    def test_create_boilerplate(
        self, construct_url, auth_cookie, test_id, success_code
    ):
        data = {
            "name": f"{test_id} created by Test",
            "description": "Testing",
        }
        response = self.session.post(
            url=construct_url(
                "gotecq.boilerplate:create-boilerplate/boilerplate"),
            json=data,
            cookies=auth_cookie,
        )
        log_response(response)
        self.result["boilerplate_id"] = (
            response.json().get("_resp")[0].get("data").get("_id")
        )
        assert response.status_code in success_code

    @pytest.mark.debug
    def test_get_boilerplate(
        self, construct_url, auth_cookie, test_id, success_code
    ):
        response = self.session.get(
            url=construct_url(
                "gotecq.boilerplate/boilerplate",
                self.result.get("boilerplate_id")
            ),
            cookies=auth_cookie,
        )
        log_response(response)
        assert response.status_code in success_code
